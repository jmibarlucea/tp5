#include "Scores.h"
#include "nana/gui.hpp"
#include "nana/gui/widgets/label.hpp"



int main()
{
	char score1[21];
	char score2[21];
	char score3[21];
	char score4[21];
	char score5[21];

	using namespace nana;

	Scores::Scores * scoreManager = new Scores::Scores();
	scoreManager->setScore(300, "AAA");
	scoreManager->setScore(250, "BBB");
	scoreManager->setScore(200, "ABC");
	scoreManager->setScore(190, "XYZ");
	scoreManager->setScore(50, "ZXY");

	form fm;

	label lb0{ fm };
	label lb1{ fm };
	label lb2{ fm };
	label lb3{ fm };
	label lb4{ fm };
	label lb5{ fm };


	lb0.caption("SCORES: ");

	sprintf(score1, "%d", scoreManager->getScore(0));
	lb1.caption(scoreManager->getName(0) + " - " + score1);

	sprintf(score2, "%d", scoreManager->getScore(1));
	lb2.caption(scoreManager->getName(1) + " - " + score2);

	sprintf(score3, "%d", scoreManager->getScore(2));
	lb3.caption(scoreManager->getName(2) + " - " + score3);

	sprintf(score4, "%d", scoreManager->getScore(3));
	lb4.caption(scoreManager->getName(3) + " - " + score4);


	sprintf(score5, "%d", scoreManager->getScore(4));
	lb5.caption(scoreManager->getName(4) + " - " + score5);

	

	

	place layout(fm);	

	layout.div("<vertical here>");	
	layout.field("here") << lb0 << lb1 << lb2 << lb3 << lb4 << lb5;
	layout.collocate();
	fm.show();
	exec();

	return 0;
}