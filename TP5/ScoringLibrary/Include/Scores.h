#ifndef SCORES_H
#define SCORES_H

#include <list>
#include <string>

namespace Scores{

	#define SIZE 20

	class Scores {
	private:
		int _scoreList[SIZE];
		std::string _nameList[SIZE];
	public:
		Scores();
		~Scores();
		void setScore(int score, std::string name);
		int getScore(int position);
		std::string getName(int position);
	};
}

#endif	